package dht

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net"
)

type Transport struct {
	bindAddr string
	parentId string
}

func MakeTransport(address, id string) *Transport {
	return &Transport{address, id}
}

// Listens for messages
func (transport *Transport) listen(queue chan<- Msg) {
	udpAddr, err := net.ResolveUDPAddr("udp", transport.bindAddr)
	if err != nil {
		fmt.Println()
		log.Printf("Transport belonging to node %x failed to resolve LISTENING address \"%s\".\n\tThe error message is \"%s\"\n", transport.parentId, udpAddr.String(), err)
		fmt.Println()
	}
	conn, err := net.ListenUDP("udp", udpAddr)
	defer conn.Close()
	for {
		data := make([]byte, 1024)
		_, err := conn.Read(data)
		//_, addr, err := conn.ReadFromUDP(data)
		if err != nil {
			//log.Println("Failed to read packet from "+addr.String()+":", err)
			continue
		}
		data = bytes.Trim(data, "\x00")
		msg := Msg{}
		err = json.Unmarshal(data, &msg)
		if err != nil {
			log.Println("Transport belonging to node %x received an un-unmarshalable message and failed.\n\t The error is \"%s\"\nThe message contains the following data:\n%s\n", transport.parentId, err, data)
			continue
		}
		if msg.Method > 0 {
			//fmt.Println("Received a message, sending to channel.")
			queue <- msg
		}
	}
}

// Sends messages through a transport
func (transport *Transport) send(msg *Msg) {
	udpAddr, err := net.ResolveUDPAddr("udp", msg.Dst)
	if err != nil {
		fmt.Println()
		log.Printf("Transport belonging to node %x failed to resolve TARGET address \"%s\".\n\tThe error message is \"%s\"\n\tThe message contains the following data:\n%s\n", transport.parentId, udpAddr.String(), err, msg)
		fmt.Println()
		return
	}
	conn, err := net.DialUDP("udp", nil, udpAddr)
	if err != nil {
		fmt.Println()
		log.Printf("Transport belonging to node %x failed to dial address \"%s\".\n\tThe error message is \"%s\"\n", transport.parentId, udpAddr.String(), err)
		fmt.Println()
		return
	}
	defer conn.Close()
	data, err := json.Marshal(msg)
	if err != nil {
		log.Println("Transport belonging to node %x tried to send an unmarshalable message and failed.\n\t The error is \"%s\"\nThe message contains the following data:\n%s\n", transport.parentId, err, msg)
	}
	if (msg.Method & MTH_STABILIZE) > 0 {
		//log.Println("Message Payload: " + msg.Payload)
	}
	_, err = conn.Write(data)
}
