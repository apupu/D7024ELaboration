package dht

import (
	"encoding/json"
	"fmt"
	"log"
)

type Msg struct {
	Payload string `json:"payload"`
	Src     string `json:"src"`
	Dst     string `json:"dst"`
	Method  uint8  `json:"mth"`
	Traffic uint8  `json:"trf"`
	Key     int    `json:"key"`
}

func (m *Msg) GetContact() (out Contact, ok bool) {
	ok = true
	err := json.Unmarshal([]byte(m.Payload), &out)
	if err != nil {
		log.Printf("Failed to unmarshal message \"%v\"!", m.Payload)
		ok = false
	}
	return
}

func (m Msg) String() string {
	return fmt.Sprintf("Msg{\n\tSrc:\t\"%s\"\n\tDst:\t\"%s\"\n\tMth: \t%b\n\tTrf:\t%b\n\tKey:\t%d\n\tPayload: \"%s\"\n}", m.Src, m.Dst, m.Method, m.Traffic, m.Key, m.Payload)
}
